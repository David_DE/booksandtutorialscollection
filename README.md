BooksAndTutorialsCollection
===========================

Just a collection of stuff I created from tutorials and/or books.

##Books

###Android Programming - The Big Nerd Ranch Guide
By Bill Phillips and Brian Hardy
http://www.bignerdranch.com/book/android_the_big_nerd_ranch_guide

###Beginning Android Games 
By Mario Zechner
http://www.apress.com/9781430230427
