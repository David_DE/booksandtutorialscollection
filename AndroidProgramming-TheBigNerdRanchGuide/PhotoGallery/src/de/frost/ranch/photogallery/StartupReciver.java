package de.frost.ranch.photogallery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class StartupReciver extends BroadcastReceiver {
	private static final String TAG = "StartupReciver";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i(TAG, "Recived brodcast intent: " + intent.getAction());
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		boolean isOn = prefs.getBoolean(PollService.PREF_IS_ALARM_ON, false);
		PollService.setServiceAlarm(context, isOn);
	}

}
