package de.frost.ranch.photogallery;

public class GalleryItem {
	private String mCaption;
	private String mId;
	private String mUrl;
	private String mOwner;
	
	public String getOwner() {
		return mOwner;
	}

	public void setOwner(String owner) {
		mOwner = owner;
	}
	
	public String getPhotoPageUrl() {
		return "http://www.flickr.com/photos/" + mOwner + "/" + mId;
	}

	public String toString() {
		return mCaption;
	}

	public String getCaption() {
		return mCaption;
	}

	public String getId() {
		return mId;
	}

	public String getUrl() {
		return mUrl;
	}

	public void setCaption(String caption) {
		mCaption = caption;
	}

	public void setId(String id) {
		mId = id;
	}

	public void setUrl(String url) {
		mUrl = url;
	}

}
