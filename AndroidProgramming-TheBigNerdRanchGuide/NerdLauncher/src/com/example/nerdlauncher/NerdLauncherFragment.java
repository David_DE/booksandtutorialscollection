package com.example.nerdlauncher;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class NerdLauncherFragment extends ListFragment {
	private static final String TAG = "NerdLauncherFragment";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Intent startIntent = new Intent(Intent.ACTION_MAIN);
		startIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		
		PackageManager pm = getActivity().getPackageManager();
		List<ResolveInfo> activities = pm.queryIntentActivities(startIntent, 0);
		
		Log.i(TAG, "Activities: " + activities.size());
		
		Collections.sort(activities, new Comparator<ResolveInfo>() {
			PackageManager pm = getActivity().getPackageManager();
			public int compare(ResolveInfo a, ResolveInfo b) {				
				return String.CASE_INSENSITIVE_ORDER.compare(a.loadLabel(pm).toString(), b.loadLabel(pm).toString());
			}
			
		});
		
		ArrayAdapter<ResolveInfo> adapter = new ArrayAdapter<ResolveInfo>(getActivity(), android.R.layout.simple_list_item_1, activities) {
			public View getView(int pos, View convertView, ViewGroup parent) {
				PackageManager pm = getActivity().getPackageManager();
				
				View v = super.getView(pos, convertView, parent);
				TextView tv = (TextView)v;
				ResolveInfo ri = getItem(pos);
				tv.setText(ri.loadLabel(pm).toString());
				return v;
			}
			
		};
		
		setListAdapter(adapter);
		

	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		ResolveInfo resolveInfo = (ResolveInfo)l.getAdapter().getItem(position);
		ActivityInfo activityInfo = resolveInfo.activityInfo;
		
		if (activityInfo == null) return;
		
		Intent i = new Intent(Intent.ACTION_MAIN);
		i.setClassName(activityInfo.applicationInfo.packageName, activityInfo.name);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		startActivity(i);
	}
	

}
