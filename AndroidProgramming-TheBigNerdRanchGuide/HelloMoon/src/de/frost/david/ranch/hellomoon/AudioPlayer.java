package de.frost.david.ranch.hellomoon;

import android.content.Context;
import android.media.MediaPlayer;
import android.view.SurfaceHolder;

public class AudioPlayer {
	
	private MediaPlayer mPlayer;
	
	public void stop() {
		if (mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
		}
	}
	
	public void play(Context c) {
		stop();
		
		mPlayer = MediaPlayer.create(c, R.raw.one_small_step);
		mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer mp) {
				stop();				
			}
		});

		mPlayer.start();
	}
	
	public void pause() {
		if (mPlayer != null)
		mPlayer.pause();
	}
	
	public void resume() {
		if (mPlayer != null) {
			mPlayer.start();
		}
	}

	public void connect(SurfaceHolder holder) {
		mPlayer.setDisplay(holder);
	}

}
