package de.frost.david.ranch.runtracker;

import android.content.Context;
import android.location.Location;

public class TrackingLocationReciver extends LocationReciver {
	@Override
	protected void onLocationRecived(Context context, Location loc) {
		RunManager.get(context).insertLocation(loc);
	}
}
