package de.frost.david.ranch.runtracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

public class LocationReciver extends BroadcastReceiver {
	private static final String TAG = LocationReciver.class.getSimpleName();
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Location loc = (Location)intent.getParcelableExtra(LocationManager.KEY_LOCATION_CHANGED);
		
		if (loc != null) {
			onLocationRecived(context, loc);
			return;
		}
		
		if (intent.hasExtra(LocationManager.KEY_PROVIDER_ENABLED)) {
			boolean enabled = intent.getBooleanExtra(LocationManager.KEY_PROVIDER_ENABLED, false);
			onProviderEnabledChanged(enabled);
		}
	}

	protected void onProviderEnabledChanged(boolean enabled) {
		Log.d(TAG, "onProviderEnabledChanged: " + enabled);
	}

	protected void onLocationRecived(Context context, Location loc) {
		Log.d(TAG,  this + " got location " + loc.getLatitude() + " / " + loc.getLongitude());
	}
	

}
