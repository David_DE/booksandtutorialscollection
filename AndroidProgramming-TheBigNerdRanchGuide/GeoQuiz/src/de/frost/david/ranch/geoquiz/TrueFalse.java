package de.frost.david.ranch.geoquiz;

public class TrueFalse {

	private int mQuestion;
	private boolean mTrueQuestion;
	private boolean mCheatedOn;
	

	public TrueFalse(int question, boolean trueQuestion) {
		mQuestion = question;
		mTrueQuestion = trueQuestion;
		mCheatedOn = false;
	}

	public int getQuestion() {
		return mQuestion;
	}

	public void setQuestion(int question) {
		mQuestion = question;
	}

	public boolean isTrueQuestion() {
		return mTrueQuestion;
	}

	public void setTrueQuestion(boolean trueQuestion) {
		mTrueQuestion = trueQuestion;
	}
	
	public boolean isCheatedOn() {
		return mCheatedOn;
	}
	
	public void setCheatedOn(boolean cheatedOn) {
		mCheatedOn = cheatedOn;
	}
}
