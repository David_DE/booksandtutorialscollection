package de.frost.david.ranch.geoquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CheatActivity extends Activity {
	
	public static final String EXTRA_ANSWER_IS_TRUE = "de.frost.david.ranch.geoquiz.answer_is_true";
	public static final String EXTRA_ANSWER_SHOWN = "de.frost.david.ranch.geoquiz.answer_shown";
	public static final String KEY_INDEX = "KEY_INDEX";
	
	private boolean mAnswerIsTrue;
	private Button mShowButton;
	private TextView mAnswerTextView;
	private TextView mApiLevelTextView;
	private boolean mCheatShown;
	
	@Override
	public void onCreate(Bundle savedInsctanceState) {
		super.onCreate(savedInsctanceState);
		setContentView(R.layout.activity_cheat);
		
		if (savedInsctanceState != null) {
			mCheatShown = savedInsctanceState.getBoolean(KEY_INDEX);
		}
		
		mAnswerIsTrue = getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false);
		
		mAnswerTextView = (TextView) findViewById(R.id.answerTextView);
		
		setAnswerShownResult(mCheatShown);
		
		mShowButton = (Button) findViewById(R.id.showAnswerButton);
		mShowButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (mAnswerIsTrue) {
					mAnswerTextView.setText(R.string.true_button);					
				}
				else {
					mAnswerTextView.setText(R.string.false_button);					
				}
				mCheatShown = true;
				setAnswerShownResult(mCheatShown);
				
			}
		});
		
		mApiLevelTextView = (TextView) findViewById(R.id.apiLevelTextView);
		mApiLevelTextView.setText("API level "+Build.VERSION.SDK_INT);
		
		
	}
	
	private void setAnswerShownResult(boolean isAnswerShown) {
		Intent data = new Intent();
		data.putExtra(EXTRA_ANSWER_SHOWN, isAnswerShown);
		setResult(RESULT_OK, data);
	}
	
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
    	super.onSaveInstanceState(savedInstanceState);
    	Log.d(QuizActivity.TAG, "onSaveInstanceState() in CheatActivity");
    	savedInstanceState.putBoolean(KEY_INDEX, mCheatShown);
    	
    }

}
