package de.frost.david.ranch.geoquiz;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class QuizActivity extends Activity {
	
	// TestKommentar
	
	public static final String TAG = "QuizActivity";
	private static final String KEY_INDEX = "KEY_INDEX";
	private static final String KEY_INDEX_CHEATER = "KEY_INDEX_CHEATER";
	
	private Button mTrueButton;
	private Button mFalseButton;
	private Button mCheatButton;
	private ImageButton mNextButton;
	private ImageButton mPreviousButton;
	private TextView mQuestionTextView;
	
	private boolean mIsCheater;
	
	private TrueFalse[] mQuestionBank = new TrueFalse[] {
			new TrueFalse(R.string.question_oceans, true),
			new TrueFalse(R.string.question_mideast, false),
			new TrueFalse(R.string.question_africa, false),
			new TrueFalse(R.string.question_americas, true),
			new TrueFalse(R.string.question_asia, true),
	};
	
	private int mCurrentIndex = 0;
	

    @TargetApi(Build.VERSION_CODES.HONEYCOMB) // 11
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate() called"+"Cheat: "+mQuestionBank[0].isCheatedOn());
        setContentView(R.layout.activity_quiz);
        
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
        	ActionBar action = getActionBar();
        	action.setSubtitle("Bodies of Water");
        }
        
        if (savedInstanceState != null) {
        	mCurrentIndex = savedInstanceState.getInt(KEY_INDEX);
        	mIsCheater = savedInstanceState.getBoolean(KEY_INDEX_CHEATER, false);
        	
        }
        
        mQuestionTextView = (TextView) findViewById(R.id.question_text_view);
        mQuestionTextView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;
				updateQuestion();
			}
		});
        updateQuestion();
        
        mTrueButton = (Button) findViewById(R.id.true_button);
        mTrueButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				checkAnswer(true);
			}
		});
        
        mFalseButton = (Button) findViewById(R.id.false_button);
        mFalseButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				checkAnswer(false);
			}
		});
        
        mNextButton = (ImageButton) findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new OnClickListener() {
        	
        	@Override
        	public void onClick(View v) {
        		mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;
//        		mIsCheater = false;
        		updateQuestion();
        	}

        });

        mPreviousButton = (ImageButton) findViewById(R.id.previous_button);
        mPreviousButton.setOnClickListener(new OnClickListener() {
        	
        	@Override
        	public void onClick(View v) {
        		mCurrentIndex = (mCurrentIndex - 1) % mQuestionBank.length;
        		if (mCurrentIndex < 0) mCurrentIndex += mQuestionBank.length;
        		mIsCheater = false;
        		updateQuestion();
        	}
        	
        });
        
        mCheatButton = (Button) findViewById(R.id.cheat_button);
        mCheatButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(QuizActivity.this, CheatActivity.class);
				boolean answerIsTrue = mQuestionBank[mCurrentIndex].isTrueQuestion();
				i.putExtra(CheatActivity.EXTRA_ANSWER_IS_TRUE, answerIsTrue);
				startActivityForResult(i, 0);
			}
		});
        
    }
    
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
    	super.onSaveInstanceState(savedInstanceState);
    	Log.d(TAG, "onSaveInstanceState() in QuizActivity");
    	savedInstanceState.putInt(KEY_INDEX, mCurrentIndex);
    	savedInstanceState.putBoolean(KEY_INDEX_CHEATER, mIsCheater);
    	
    }

    private void updateQuestion() {
    	int question = mQuestionBank[mCurrentIndex].getQuestion();
    	mQuestionTextView.setText(question);
    }
    
    private void checkAnswer(boolean userPressedTrue) {
    	boolean answerIsTrue = mQuestionBank[mCurrentIndex].isTrueQuestion();
    	
    	int messageResId = 0;
    	
    	if (mIsCheater) {
    		messageResId = R.string.judgment_toast;
    	}
    	else {
    		if (userPressedTrue == answerIsTrue) {
    			messageResId = R.string.correct_toast;
    		}
    		else {
    			messageResId = R.string.incorrect_toast;
    		}
    	}
    	
    	Toast.makeText(this, messageResId, Toast.LENGTH_SHORT).show();
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultcode, Intent data) {
    	if (data == null) {
    		return;
    	}
    	mIsCheater = data.getBooleanExtra(CheatActivity.EXTRA_ANSWER_SHOWN, false);
//    	if (!mQuestionBank[mCurrentIndex].isCheatedOn()) {
//    		mQuestionBank[mCurrentIndex].setCheatedOn(data.getBooleanExtra(CheatActivity.EXTRA_ANSWER_SHOWN, false));
//    	}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.quiz, menu);
        return true;
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	Log.d(TAG, "onStart() called"+"Cheat: "+mQuestionBank[0].isCheatedOn());
    }
    
    @Override
    public void onPause() {
    	super.onPause();
    	Log.d(TAG, "onPase() called"+"Cheat: "+mQuestionBank[0].isCheatedOn());
    }
    
    @Override
    public void onResume() {
    	super.onResume();
    	Log.d(TAG, "onResum() called"+"Cheat: "+mQuestionBank[0].isCheatedOn());
    }
    
    @Override
    public void onStop() {
    	super.onStop();
    	Log.d(TAG, "onStop() called"+"Cheat: "+mQuestionBank[0].isCheatedOn());
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	Log.d(TAG, "onDestroy() called"+"Cheat: "+mQuestionBank[0].isCheatedOn());
    }

    
}
