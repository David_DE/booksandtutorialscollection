package de.frost.david.android.game.basics;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

public class MultiTouchTest extends Activity implements OnTouchListener {
	private StringBuilder mBuilder = new StringBuilder();
	private TextView mTextView;
	private float [] mX = new float[10];
	private float [] mY = new float[10];
	private boolean [] mTouched = new boolean [10];
	private int [] mId = new int [10];
	
	private void updateTextView() {
		mBuilder.setLength(0);
		
		for (int i = 0; i < 10; i++) {
			mBuilder.append(mTouched[i] + ", " + mId[i] + ", "
					+ mX[i] + ", " + mY[i] + "\n");
		}
		
		mTextView.setText(mBuilder.toString());
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mTextView = new TextView(this);
		mTextView.setText("Touch and drag (multiple fingers supported)!");
		mTextView.setOnTouchListener(this);
		setContentView(mTextView);
		
		for (int i = 0; i < 10; i++) {
			mId[i] = -1;
		}
		
		updateTextView();
	}

	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int action = event.getAction() & MotionEvent.ACTION_MASK;
		int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK)
				>> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
		int pointerCount = event.getPointerCount();
		
		for (int i = 0; i < 10; i++) {
			if (i >= pointerCount) {
				mTouched[i] = false;
				mId[i] = -1;
				continue;
			}
			if (event.getAction() != MotionEvent.ACTION_MOVE &&
					i != pointerIndex) {
				continue;
			}
			int pointerId = event.getPointerId(i);
			switch (action) {
				case MotionEvent.ACTION_DOWN:
				case MotionEvent.ACTION_POINTER_DOWN:
					mTouched[i] = true;
					mId[i] = pointerId;
					mX[i] = (int)event.getX(i);
					mY[i] = (int)event.getY(i);
					break;
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_POINTER_UP:
				case MotionEvent.ACTION_OUTSIDE:
				case MotionEvent.ACTION_CANCEL:
					mTouched[i] = false;
					mId[i] = pointerId;
					mX[i] = (int)event.getX(i);
					mY[i] = (int)event.getY(i);
					break;
				case MotionEvent.ACTION_MOVE:
					mTouched[i] = true;
					mId[i] = pointerId;
					mX[i] = (int)event.getX(i);
					mY[i] = (int)event.getY(i);
					break;
			}
		}
		
		updateTextView();		
		return true;
	}

	
}
