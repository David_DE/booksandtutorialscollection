package de.frost.david.android.game.basics;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

public class AccelerometerTest extends Activity implements SensorEventListener {
	private StringBuilder mBuilder = new StringBuilder();
	private TextView mTextView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mTextView = new TextView(this);
		setContentView(mTextView);
		
		SensorManager manager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
		if (manager.getSensorList(Sensor.TYPE_ACCELEROMETER).size() == 0) {
			mTextView.setText("No accelerometer installed");
		} else {
			Sensor accelerometer = manager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
			if (!manager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME)) {
				mTextView.setText("Couldn't register sensor listener");
			}
		}
		
	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		mBuilder.setLength(0);
		
		mBuilder.append("x: " + event.values[0] + ", y: " + event.values[1] + ", z: " + event.values[2]);
		mTextView.setText(mBuilder.toString());
	}
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) { }
	//TODO: test
	
}
