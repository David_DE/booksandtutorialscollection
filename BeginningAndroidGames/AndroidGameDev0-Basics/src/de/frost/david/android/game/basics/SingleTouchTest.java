package de.frost.david.android.game.basics;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

public class SingleTouchTest extends Activity implements OnTouchListener {
	private StringBuilder mBuilder = new StringBuilder();
	private TextView mTextView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mTextView = new TextView(this);
		mTextView.setText("Touch and drag (one finger only)!");
		mTextView.setOnTouchListener(this);
		setContentView(mTextView);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		mBuilder.setLength(0);

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			mBuilder.append("down, ");
			break;
		case MotionEvent.ACTION_MOVE:
			mBuilder.append("move, ");
			break;
		case MotionEvent.ACTION_CANCEL:
			mBuilder.append("cancel, ");
			break;
		case MotionEvent.ACTION_UP:
			mBuilder.append("up, ");
			break;
		}
		
		mBuilder.append(event.getX() + ", " + event.getY());
		String text = mBuilder.toString();
		Log.d("TouchTest", text);
		mTextView.setText(text);

		return true;
	}

}
