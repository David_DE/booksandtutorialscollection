package de.frost.david.android.game.basics;

import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Window;
import android.view.WindowManager;

public class FullScreenTest extends SingleTouchTest {
	private WakeLock mWakeLock;
	
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		mWakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "my Lock");
		
		super.onCreate(savedInstanceState);
		
		
	}
	
	@Override
	public void onResume() {
		mWakeLock.acquire();
		super.onResume();
		
	}
	
	@Override
	public void onPause() {
		mWakeLock.release();
		super.onPause();
		
	}

}
