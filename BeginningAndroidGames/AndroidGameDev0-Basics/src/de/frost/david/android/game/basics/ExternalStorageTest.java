package de.frost.david.android.game.basics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.widget.TextView;

public class ExternalStorageTest extends Activity {
	private TextView mTextView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mTextView = new TextView(this);
		setContentView(mTextView);
		
		String state = Environment.getExternalStorageState();
		
		if (!state.equals(Environment.MEDIA_MOUNTED)) {
			mTextView.setText("No external storage mounted");
		} else {
			File externalDir = Environment.getExternalStorageDirectory();
			File textFile = new File(externalDir.getAbsolutePath() + File.separator + "text.txt");
			
			try {
				writeTextFile(textFile, "This is a test. Roger");
				String text = readTextFile(textFile);
				mTextView.setText(text);
				
				if (!textFile.delete()) {
					mTextView.setText("Couldn't remove temporary file");
				}
			} catch (IOException e) {
				mTextView.setText("Something went wrong" + e.getMessage());
			}
		}
		
	}

	private String readTextFile(File file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		StringBuilder text = new StringBuilder();
		String line;
		
		while ((line = reader.readLine()) != null) {
			text.append(line + "\n");
		}
		reader.close();
		
		return text.toString();
	}

	private void writeTextFile(File file, String string) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));	
		writer.write(string);
		writer.close();
	}

}
