package de.frost.david.android.game.basics;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.TextView;

public class KeyTest extends Activity implements OnKeyListener {
	private StringBuilder mBuilder = new StringBuilder();
	private TextView mTextView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mTextView = new TextView(this);
		mTextView.setText("Press keys (if you have some)");
		mTextView.setOnKeyListener(this);
		mTextView.setFocusableInTouchMode(true);
		
		setContentView(mTextView);
	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		mBuilder.setLength(0);
		
		switch (event.getAction()) {
		case KeyEvent.ACTION_DOWN:
			mBuilder.append("down, ");			
			break;
		case KeyEvent.ACTION_UP:
			mBuilder.append("up, ");
			break;
		}
		mBuilder.append(event.getKeyCode() + ", " + (char) event.getUnicodeChar());
		String text = mBuilder.toString();
		Log.d("KeyTest", text);
		mTextView.setText(text);
		
		return event.getKeyCode() != KeyEvent.KEYCODE_BACK;
	}

}
