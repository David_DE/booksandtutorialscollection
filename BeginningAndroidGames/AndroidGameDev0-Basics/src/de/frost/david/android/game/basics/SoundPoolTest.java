package de.frost.david.android.game.basics;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

public class SoundPoolTest extends Activity implements OnTouchListener {
	private TextView mTextView;
	private SoundPool mSoundPool;
	private int explosionId = -1;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mTextView = new TextView(this);
		mTextView.setOnTouchListener(this);
		setContentView(mTextView);
		
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		mSoundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
		
		try {
			AssetManager assetManager = getAssets();
			AssetFileDescriptor descriptor = assetManager.openFd("explosion.ogg");
			explosionId = mSoundPool.load(descriptor, 1);
		} catch (IOException e) {
			mTextView.setText("Couldn't load sound effect from asset, " + e.getMessage());
		}
		
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		mSoundPool.unload(explosionId);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_UP) {
			if (explosionId != -1) {
				mSoundPool.play(explosionId, 1, 1, 0, 0, 1);
			}
		}
		return true;
	}

}
