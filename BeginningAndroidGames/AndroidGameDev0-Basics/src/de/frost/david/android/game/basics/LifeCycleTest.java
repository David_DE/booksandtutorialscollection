package de.frost.david.android.game.basics;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class LifeCycleTest extends Activity {
	private StringBuilder mBuilder = new StringBuilder();
	private TextView mTextView;
	
	private void log(String text) {
		Log.d("LifeCycleTest", text);
		mBuilder.append(text);
		mBuilder.append('\n');
		mTextView.setText(mBuilder.toString());
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mTextView = new TextView(this);
		mTextView.setText(mBuilder.toString());
		setContentView(mTextView);
		log("created");
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		log("resumed");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		log("paused");
		
		if (isFinishing()) {
			log("finishing");
		}
	}
	

}
