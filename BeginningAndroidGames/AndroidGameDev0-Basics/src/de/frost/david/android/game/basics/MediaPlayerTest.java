package de.frost.david.android.game.basics;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.TextView;

public class MediaPlayerTest extends Activity {
	private TextView mTextView;
	private MediaPlayer mMediaPlayer;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mTextView = new TextView(this);
		setContentView(mTextView);
		
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		mMediaPlayer = new MediaPlayer();
		
		try {
			AssetManager assetManager = getAssets();
			AssetFileDescriptor descriptor = assetManager.openFd("music.ogg");
			mMediaPlayer.setDataSource(descriptor.getFileDescriptor(), 
					descriptor.getStartOffset(), descriptor.getLength());
			mMediaPlayer.prepare();
			mMediaPlayer.setLooping(true);
		} catch (IOException e) {
			mTextView.setText("Couldn't load music file, " + e.getMessage());
		}
		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if (mMediaPlayer != null) {
			mMediaPlayer.start();
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		if (mMediaPlayer != null) {
			mMediaPlayer.stop();
			
			if (isFinishing()) {
				mMediaPlayer.stop();
				mMediaPlayer.release();
			}
		}
	}


}
