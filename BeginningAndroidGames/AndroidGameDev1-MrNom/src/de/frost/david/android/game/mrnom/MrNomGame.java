package de.frost.david.android.game.mrnom;

import de.frost.david.android.game.framework.Screen;
import de.frost.david.android.game.framework.impl.AndroidGame;

public class MrNomGame extends AndroidGame {
	
	public Screen getStartScreen() {
		return new LoadingScreen(this);
	}

}
