package de.frost.david.android.game.framework.impl;

import android.media.SoundPool;
import de.frost.david.android.game.framework.Sound;

public class AndroidSound implements Sound {
	private int mSoundId;
	private SoundPool mSoundPool;

	public AndroidSound(SoundPool soundPool, int soundId) {
		this.mSoundId = soundId;
		this.mSoundPool = soundPool;
	}

	@Override
	public void play(float volume) {
		mSoundPool.play(mSoundId, volume, volume, 0, 0, 1);		
	}

	@Override
	public void dispose() {
		mSoundPool.unload(mSoundId);		
	}

}
