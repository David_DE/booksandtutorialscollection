package de.frost.david.android.game.framework.impl;

import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import de.frost.david.android.game.framework.Music;

public class AndroidMusic implements Music, OnCompletionListener {
	private MediaPlayer mMediaPlayer;
	private boolean mIsPrepared = false;

	public AndroidMusic(AssetFileDescriptor assetDescriptor) {
		mMediaPlayer = new MediaPlayer();
		
		try {
			mMediaPlayer.setDataSource(assetDescriptor.getFileDescriptor(), 
					assetDescriptor.getStartOffset(), 
					assetDescriptor.getLength());
			mMediaPlayer.prepare();
			mIsPrepared = true;
			mMediaPlayer.setOnCompletionListener(this);
		} catch (Exception e) {
			throw new RuntimeException("Couldn't load music");
		}
	}

	@Override
	public void play() {
		if (mMediaPlayer.isPlaying())
			return;
		try {
			synchronized (this) {
				if (!mIsPrepared)
					mMediaPlayer.prepare();
				mMediaPlayer.start();
			}
 		} catch (IllegalStateException e) {
 			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void stop() {
		mMediaPlayer.stop();
		
		synchronized (this) {
			mIsPrepared = false;
		}
	}

	@Override
	public void pause() {
		if (mMediaPlayer.isPlaying())
			mMediaPlayer.pause();
	}

	@Override
	public void setLooping(boolean looping) {
		mMediaPlayer.setLooping(looping);
	}

	@Override
	public void setVolume(float volume) {
		mMediaPlayer.setVolume(volume, volume);
	}

	@Override
	public boolean isPlaying() {
		return mMediaPlayer.isPlaying();
	}

	@Override
	public boolean isStopped() {
		return !mIsPrepared;
	}

	@Override
	public boolean isLooping() {
		return mMediaPlayer.isLooping();
	}

	@Override
	public void dispose() {
		if (mMediaPlayer.isPlaying()) {
			mMediaPlayer.stop();
		}
		
		mMediaPlayer.release();
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		synchronized (this) {
			mIsPrepared = false;
		}
	}

}
