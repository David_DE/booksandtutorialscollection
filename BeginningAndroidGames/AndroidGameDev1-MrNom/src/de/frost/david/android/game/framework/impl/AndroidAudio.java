package de.frost.david.android.game.framework.impl;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;
import de.frost.david.android.game.framework.Audio;
import de.frost.david.android.game.framework.Music;
import de.frost.david.android.game.framework.Sound;

public class AndroidAudio implements Audio{
	private AssetManager mAssets;
	private SoundPool mSoundPool;
	
	public AndroidAudio(Activity activity) {
		activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		this.mAssets = activity.getAssets();
		this.mSoundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
	}

	@Override
	public Music newMusic(String filename) {
		try {
			AssetFileDescriptor descriptor = mAssets.openFd(filename);
			return new AndroidMusic(descriptor);
		} catch (IOException e) {
			throw new RuntimeException("Couldn't load music '" + filename + "'");
		}
	}

	@Override
	public Sound newSound(String filename) {
		try {
			AssetFileDescriptor descriptor = mAssets.openFd(filename);
			int soundId = mSoundPool.load(descriptor, 0);
			return new AndroidSound(mSoundPool, soundId);
		} catch (Exception e) {
			throw new RuntimeException("Couldn't load sound '" + filename + "'");
		}
	}

}
