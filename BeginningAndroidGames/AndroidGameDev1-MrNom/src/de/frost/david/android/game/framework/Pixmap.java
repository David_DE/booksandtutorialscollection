package de.frost.david.android.game.framework;

import de.frost.david.android.game.framework.Graphics.PixmapFormat;

public interface Pixmap {
	public int getWidth();
	public int getHeight();
	
	public PixmapFormat getFormat();
	public void dispose();

}
